<?php
  include("functionfiles/index.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href="logsign.php">Sign IN / Sign UP</a></li>
              <li><a href="" data-toggle="modal" data-target="#guidemodal">Need Guidance</a></li>
                


            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center"  data-aos="fade-up">
    <div class="container">
      <div class="row justify-content-start">
        <div class="col-xl-6">
          <!-- <h1 style="text-align: left;">Marketplace for all <br>your project needs</h1> -->
       
            <?php 
            
            while($row=mysqli_fetch_array($que))
            {
              echo  "<h1 style='text-align: left;'>".$row['tagline']."</h1>";
            }
            ?>

          <!-- <h2 style="text-align: left;">That deliver project at your doorstep</h2> -->
          <!-- <?php 
            
            while($row1=mysqli_fetch_array($que1))
            {
              echo  "<h2 style='text-align: left;'>".$row1['subtagline']."</h2>";
            }
            ?> -->
          <div class="row">
            <div class="col-6">
              <a href="profile.php" class="btn start-now" >Start now</a>
            </div>
            <div class="col-4"></div>
            <div class="col-2"></div>
          </div>
          
        </div>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <!-- <section id="about" class="about">
      <div class="container">

     

      </div>
    </section> -->
    <!-- End About Us Section -->

    <section id="features" class="features" data-aos="fade-left">
      <div class="container">
        <div class="section-title">
          <h2 style="color: black;">Why Project Bazar?</h2>
        </div>
        <div class="row">
         
          <div class=" col-lg-6 image order-1 order-lg-2" style='background-image: url("assets/img/features.jpg");' >
            
          </div>



       
          <div class="col-lg-6 order-2 order-lg-1">
            <div class="icon-box mt-5 mt-lg-0">
              <i class="bx bx-receipt"  style="margin-right: 1rem;"></i>
            
                <p style="color: black; padding: 0;margin: 0; padding-top: .5rem;font-weight: bold;font-size: 1.1rem;">Get Guidance Your Project Journey</p>
 
            </div>
            <div class="icon-box mt-5">
              <i class="bx bx-cube-alt"  style="margin-right: 1rem;"></i>
              <p style="color: black; padding: 0;margin: 0; padding-top: .5rem;font-weight: bold;font-size: 1.1rem;">Huge Saving At Lowest Price</p>
            </div>
           
            <div class="icon-box mt-5">
              <i class="bx bx-shield"  style="margin-right: 1rem;"></i>
              <p style="color: black; padding: 0;margin: 0; padding-top: .5rem;font-weight: bold;font-size: 1.1rem;">Easy To Get Sponsor Project</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Features Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients" data-aos="flip-up">
      <div class="container">

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-1.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-2.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-3.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-4.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-5.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-6.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-7.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="./assets/img/clients/client-8.png" class="img-fluid" alt="">
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts" data-aos="fade-down">
      <div class="container">


        <div class="text-center title">
          <h3>What we have achieved so far</h3>
        </div>

        
        <div class="row counters">

        <?php 
            
            while($row2=mysqli_fetch_array($que2))
            {
              
            
            ?>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?php echo  $row2['clients']; ?></span>
            <p>Clients</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?php echo  $row2['projects']; ?></span>
            <p>Projects</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?php echo  $row2['hours']; ?></span>
            <p>Hours Of Support</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up"><?php echo  $row2['worker']; ?></span>
            <p>Hard Workers</p>
          </div>
          <?php }  ?>
        </div>

      </div>
    </section><!-- End Counts Section -->

    <section>
      <div class="container">
        <div class="section-title">
          <h2 style="color: black;">Projects</h2>
        </div>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
            
                <div class="col-4 card" style="margin-right: .5rem;margin-left:0.4rem;">

                  <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">Protecting Induction Motor From Phase & Temperature</h5>

                  <img src="./assets/img/photo0.1.jpg" style="height: 40vh; width:40vh">
                  <br>
                  <!-- <p style="color: black;"></p>
                  <p style="color: black;">Price</p> -->
                </div>
                <div class="col-4 card" style="margin-right: .5rem;margin-left: -1rem;">
                  <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">RFID RC522 Attendance System Using Arduino with Data Logger</h5>
                  <img src="./assets/img/photo0.2.jpg" style="height: 40vh; width:40vh">
                  <br>
                  <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                  <p>Price</p> -->
                </div>
                <div class="col-4 card" style="margin-right: .5rem;margin-left: -1rem;">
                  <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">HOME APPLIANCES CONTROLLING USING ANDROID MOBILE VIA BLUETOOTH</h5>
                  <img src="./assets/img/photo0.3.jpg" style="height: 40vh; width:40vh">

                  <br>
                  <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                  <p>Price</p> -->
                </div>
                
              
              </div>
            </div>
            <div class="carousel-item">
   
            <div class="row">
          
              <div class="col-4 card" style="margin-right: .5rem;margin-left:0.4rem;">
              <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">Sentiment analysis for product rating.</h5>
                  <img src="./assets/img/photo0.4.jpg" style="height: 40vh; width:40vh;margin:1rem">
                <br>
                <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                <p>Price</p> -->
              </div>
              <div class="col-4 card" style="margin-right: .5rem;margin-left: -1rem;">
              <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">Active Headlight Steering Control with Brightness Control</h5>
                  <img src="./assets/img/photo0.6.jpg" style="height: 40vh; width:40vh">

                <br>
                <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                <p>Price</p> -->
              </div>
              <div class="col-4 card" style="margin-right: .5rem;margin-left: -1rem;">
              <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">Pneumatic Vehicle</h5>
                  <img src="./assets/img/photo0.9.jpg" style="height: 40vh; width:40vh;margin-top:1.5rem">

                <br>
                <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                <p>Price</p> -->
              </div>
            
        </div>
            </div>
            <div class="carousel-item">
              <div class="row">
          
                <div class="col-4 card" style="margin-right: .5rem;margin-left:0.4rem;">
                  <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">Image encryption using AES algorithm</h5>
                  <img src="./assets/img/photo0.11.jpg" style="height: 40vh; width:40vh">

                  <br>
                  <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                  <p>Price</p> -->
                </div>
                <div class="col-4 card" style="margin-right: .5rem;margin-left: -1rem;">
                  <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">Android-based Function Generator</h5>
                  <img src="./assets/img/photo0.12.jpg" style="height: 40vh; width:40vh">

                  <br>
                  <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                  <p>Price</p> -->
                </div>
                <div class="col-4 card" style="margin-right: .5rem;margin-left: -1rem;">
                  <h5 style="color: black;margin-top: 1rem; text-transform:uppercase">Beam Engine</h5>
                  <img src="./assets/img/photo0.10.jpg" style="height: 40vh; width:40vh;;margin-top:1.5rem">

                  <br>
                  <!-- <p style="color: black;">DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription</p>
                  <p>Price</p> -->
                </div>
              
              </div>
            </div>
          </div>
          
          <a style="" class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon bg-dark " aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a style="" class="carousel-control-next " href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon bg-dark" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
     
      </div>
    </section>

    <div class="modal fade" id="guidemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <div class="h4 text-dark">Need Guidance</div>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        </div>
        <p class="text-dark">Contact this Number</p>
        <p class="text-dark">+91 7001811025</p>
      </div>
     
    </div>
  </div>
</div>
  </main><!-- End #main -->
  <!-- ======= Footer ======= -->
<?php
  include("functionfiles/footer.php");

?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script>
  AOS.init();
</script>

</body>

</html>