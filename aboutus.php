<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href="logsign.php">Sign IN / Sign UP</a></li>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about" style="color: black;">
      <div class="container">
          <div class="row" style="margin-top: 1rem;">
              <div class="col-12">
              <!-- <div class="h4 text-center">Meet the term</div> -->
              </div>
          </div>
          <div class="row" style="">
            <div class="col-12">
            <p class="text-center">Our Story</p>
            </div>
          </div>
        <div class="row">
          <div class="col-12">
            <p class="text-center">We saw a challenge</p>        
          </div>
        </div>

        <p>A student face challenge of Finding a trusted company, 
            True value for the project, Project at your doorstep, Patent and Research
             paper registration But where can you find these resources, all in one place?
        </p>
        <div class="row" >

            <div class="col-12" style="text-align: center;">
                <h3>How do you find a company with the qualities and
                    experience to tackle your project?
                </h3>
            </div>
           
        </div>
       
        <div style="border-bottom: 1px solid black; margin-top: 1rem;"></div>
        
        <p style="margin-top: 1rem;"> We Built a Solution</p>

        <p>A platform for all the student needs. We connect you to the certified companies, 
            with the insights and analysis of the project value, secure delivery of your project,
             and tackle challenges with confidence. We challenge conventional methods to bring
              innovative solutions to the student enabling them to achieve sustainable advantage. 
              We bring together the right people, just enough Process, and leading Technology to
               refine and simplify an organization’s ability to successfully deliver projects.
        </p>
        <div style="border-bottom: 1px solid black; margin-top: 1rem;"></div>


      </div>
    </section>
  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>