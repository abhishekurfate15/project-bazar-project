<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li class="active"><a href="logsign.php">Sign IN / Sign UP</a></li>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main" style="background-color:black">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: white;margin-top: 3rem;">
      <h4 class="text-center text-light">ADMIN PAGE</h4>
      <div class="container signpage" style="margin-top: 1rem; border:1px solid white">
        
          <div class="row card mb-3" style="margin-top: 4vh;margin-left:.1px;margin-right:.1px">
            <div class="col-12 signin">
              <form style="margin:1rem" action="functionfiles/adminfunc.php" method="post">
               
                  <div class="form-group ">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" id="inputEmail4" name="loginemail" placeholder="Email" style="border:1px solid gray">
                  </div>
                  <div class="form-group">
                    <label for="inputPassword4">Password</label>
                    <input type="password" class="form-control" id="inputPassword4" name="loginpass" placeholder="Password"style="border:1px solid gray">
                  </div>

                  <button type="submit" class="btn bg-dark text-light" name="admin-signin-btn">Sign In</button>
                  <button type="submit" class="btn bg-light " style="border:1px solid black">Cancel</button>

              </form>
            </div>
            

          </div>
      </div>
    <div class="container" style="color: black;margin-top: 3rem;">

    </div>
  </section>


<!-- Modal -->
<div class="modal fade" id="errormodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
      
        <img src="assets/img/duplicate.png" style="max-width:100%">
      </div>
     
    </div>
  </div>
</div>
  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
 
  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <!-- <script src="myscript.js"></script> -->
 
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>