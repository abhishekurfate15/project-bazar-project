<!DOCTYPE html>
<?php
  include("functionfiles/userfunc.php");
?>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href="profile.php">Account</a></li>
              <li><a href="logout.php">Logout</a></li>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->

 
  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
        <div class="container-fluid signpage" style="margin-top: 1rem;">
           <div class="row">
               <div class="col-1">
                
                <div class="row card myprofile bg-dark" style="padding-right: 1vh;">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="profile.php" class="btn text-light nav-link">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a href="myproject.php" class="btn text-light nav-link">Projects</a>
                        </li>
                        <li class="nav-item">
                            <a href="prohistory.php" class="btn text-light nav-link">History</a>
                        </li>
                        <li class="nav-item">
                            <a href="mypayment.php" class="btn text-light nav-link">Payments</a>
                        </li>
                        <li class="nav-item">
                            <a href="logout.php" class="btn text-light nav-link">Logout</a>
                        </li>
                        <li class="nav-item">
                            <a href="faq.php" class="btn text-light nav-link">FAQ</a>
                        </li>
                        <li></li>
                    </ul>
                </div>
               </div>
               <div class="col-1"></div>
               <div class="col-10">
                    <h4 class="text-center">EDIT PROFILE</h4>

                <div class="row card" style="margin-right: 1vh;">
         <?php 
              while ($row6=mysqli_fetch_array($que)){  
                  $myid =  $row6['uid'];
         
         ?>
                    <div class="col-12">
                      <form style="margin:1rem"  action="" method="post">
                        <div class="form-row">
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control" name="uid" value="<?php echo $row6['uid']?>" id="inputPassword4" placeholder="mid">
                          </div>
                          <div class="form-group col-md-4">
                            <input type="text" class="form-control" name="fname" value="<?php  echo $row6['fname']?>" id="inputEmail4" placeholder="First Name">
                          </div>
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control" name="lname" value="<?php echo $row6['lname']?>" id="inputPassword4" placeholder="Last Name">
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" value="<?php echo $row6['email']?>" id="inputEmail4" placeholder="Email">
                          </div>
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control" name="pass" value="<?php echo $row6['pass']?>" id="inputPassword4" placeholder="Password">
                          
                          </div>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" name="address" value="<?php echo $row6['address']?>" id="inputAddress" placeholder="1234 Main St">
                        </div>
                          <div class="form-group">
                          <input type="text" class="form-control" name="mobile" value="<?php echo $row6['mobile']?>" id="inputAddress" placeholder="Contact">
                        </div>
                      
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control" name="category" value="<?php echo $row6['category']?>" id="inputCity" placeholder="Become">
                            <!-- <select id="inputState" class="form-control" name="category" style="border:1px solid gray" required>
                              <option selected><?php echo $row6['category']?></option>
                              <option>User</option>
                              <option>Company</option>
                            </select> -->
                          </div>
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control" name="city" value="<?php echo $row6['city']?>" id="inputCity" placeholder="City">
                          </div>
                         
<?php } ?>                  
                        </div>
                  
                        <button type="submit" class="btn bg-dark text-light" name="updaterecord_user">Update</button>
                        <button type="submit" class="btn bg-light " style="border:1px solid black">Cancel</button>
                      </form>
                    </div>
      
                  </div>
               </div>
           </div>
           
        </div>
      
    </section>
    <!-- ======= Footer ======= -->
    <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
 
  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="myscript.js"></script>
 
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>