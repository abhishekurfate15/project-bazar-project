<!DOCTYPE html>
<html lang="en">
<?php
  include("functionfiles/requestform.php");
?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href='profile.php'>Account</a></li>
              <li><a href='logout.php'>Logout</a></li>
              
            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
      
        <div class="container signpage">
            <h4 class="text-center">REQUEST PAGE</h4>
            <div class="row card" style="margin-top: 4vh;">
       
              <div class="col-12 signup">
              
            
                <form style="margin:1rem" action="functionfiles/sendrequest.php" method="post">
                <?php 
                    while ($row9=mysqli_fetch_array($resultdetails)){  
                         
                 ?>
                  <div class="form-group">                    
                      <label for="inputEmail4">Company Id</label>
                      <input type="text" class="form-control" id="inputEmail4" name="cid" value="<?php echo $row9['cid']; ?>">
                    </div>
                    <div class="form-group">                    
                      <label for="inputEmail4">Company Name</label>
                      <input type="text" class="form-control" id="inputEmail4" name="company_name" value="<?php echo $row9['company_name']; ?>">
                    </div>
                    
                    <div class="form-group">                    
                      <label for="inputEmail4">Full Name</label>
                      <input type="text" name="fullname" class="form-control" id="inputEmail4" placeholder="Please Enter Your Name">
                    </div>
                  
                    <div class="form-group">
                      <label for="inputAddress">Project Title</label>
                      <input type="text" name="projecttitle" class="form-control" id="inputAddress" placeholder="Please Enter Project Title">
                    </div>

                    <div class="form-group">
                      <label for="inputAddress2">Project Abstract</label>
                      <textarea class="form-control" name="projectabstract" id="exampleFormControlTextarea1" rows="3" placeholder="Please Enter Project Abstract"></textarea>
                    </div>

                    <div class="form-group">
                      <label for="inputAddress2">Add Image / Document</label>
                      <input type="file" name="myfile" class="form-control" id="inputAddress" placeholder="Please Enter Project Title">

                    </div>

                      <button type="submit" class="btn bg-dark text-light" name="sendrequest">Send</button>

                      <input type="submit" class="btn btn-light" style="border: 1px solid black;" value="Cancel">
                      <?php }?>

                </form>
              </div>

            </div>
        </div>
      <div class="container" style="color: black;margin-top: 3rem;">
 
      </div>
    </section>
   <!-- ======= Footer ======= -->
   <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
 
  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
 
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>