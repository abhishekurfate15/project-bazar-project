  <footer id="footer">
    <div class="container">
   
   
    
       
        <div class="row">
            <div class="col-3" style="margin-left: 1rem;text-align: left;">
                <h4>About Project Bazar</h4>

                <i class="bx bx-chevron-right"></i> <a href="aboutus.php">About Us</a><br>
                <i class="bx bx-chevron-right"></i> <a href="logsign.php">Company Registration</a><br>
                <i class="bx bx-chevron-right"></i> <a href="service.php">Our Service</a><br>
                <i class="bx bx-chevron-right"></i> <a href="termcondition.php">Term & Conditions</a><br>
                <i class="bx bx-chevron-right"></i> <a href="#">Blogs</a><br>
                <i class="bx bx-chevron-right"></i> <a href="policy.php">Privacy</a><br>
                <i class="bx bx-chevron-right"></i> <a href="contactus.php">Contact Us</a>
            </div>       
       
            <div class="col-4" style="margin-left: 1rem;text-align: left;">
                
                <h4>Project By Location</h4>

                <i class="bx bx-map" style="font-weight: 100;font-size: 1.2rem;"></i> <a href="./projects.php"> Pune</a><br>
                <i class="bx bx-map" style="font-weight: 100;font-size: 1.2rem;"></i><a href="./projects.php"> Mumbai</a><br>
                <i class="bx bx-map" style="font-weight: 100;font-size: 1.2rem;"></i><a href="./projects.php"> Aurangabad</a><br>
                <i class="bx bx-map" style="font-weight: 100;font-size: 1.2rem;"></i><a href="./projects.php"> Nasik</a><br>



            
            </div>

            <div class="col-4" style="text-align: left">
                <h4>Project By Stream</h4>
        
                <i class="bx bx-chevron-right"></i> <a href="./projects.php">Computer Science Engineering</a><br>
                <i class="bx bx-chevron-right"></i> <a href="./projects.php">Electrical Engineering</a><br>
                    <i class="bx bx-chevron-right"></i> <a href="./projects.php">Electronic & Tele. Engineering</a><br>
                    <i class="bx bx-chevron-right"></i> <a href="./projects.php">Marketing</a><br>
                    <i class="bx bx-chevron-right"></i> <a href="./projects.php">Mechanical Engineering</a><br>
                    <i class="bx bx-chevron-right"></i> <a href="./projects.php">MBA / BBA </a><br>
                    <i class="bx bx-chevron-right"></i> <a href="./projects.php">ME / MTECH</a><br>
                    <i class="bx bx-chevron-right"></i> <a href="./projects.php">MCA / BCS / BCA</a><br>
                
            
    
    
            </div>
        </div>

   <div class="row" style="margin-top:2rem;">
     <div class="col-2">
       <div class="row">
      
          <img src="./assets/img/FOOTERLOGO.png" style="width:14%;height:4vh;margin-top: .3rem;margin-right: .2rem;">
       
          <p style="margin-top: .5rem;">Project Bazar</p>
       </div>
     
      
     </div>
     <div class="col-5">
      <div class="copyright" style="margin-top:.5rem;">
        &copy; Copyright <strong><span>Project Bazar 2021</span></strong>. All Rights Reserved
      </div>
     </div>
     <div class="col-5">
      <div class="social-links">
        <a href="https://twitter.com/ProjectBazar" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="https://www.facebook.com/Project-Bazar-119213346659992" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="https://www.instagram.com/projectbazar02/" class="instagram"><i class="bx bxl-instagram"></i></a>
      
        <a href="https://www.linkedin.com/company/69510937/" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    
     </div>
    
   </div>
     
    
    </div>
  </footer>