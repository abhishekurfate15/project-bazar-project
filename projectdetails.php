<!DOCTYPE html>
<?php
  include("functionfiles/companyportfoliodetails.php");
?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <?php 
              session_start();
              if(!isset($_SESSION["email"])){
                  
                  echo "<li><a href='logsign.php'>Sign IN / Sign UP</a></li>";
                }
                else{
                  echo "<li><a href='profile.php'>Account</a></li>";
                  echo "<li><a href='logout.php'>Logout</a></li>";

                }
              ?>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
       
        <div class="container-fluid">
          <h4 class="text-center">COMPANY DETAILS</h4>
          <diiv class="row">
          <?php 
              while ($row9=mysqli_fetch_array($resultdetails)){  
                         
          ?>
            <div class="col">
              <div class="card" style="padding:1rem;border-radius: 4px;margin-right: .9rem;margin-left: 1rem;" >
                <div class="row">
                  <div class="col-3">
                    <h4><?php echo $row9['company_name']; ?> </h4>
                  </div>
                  <div class="col-5"> </div>
                  <div class="col-4">
                    <img src="./functionfiles/images/<?php echo $row9['company_logo']; ?> " style="max-width:20%;float:right;border-radius:50px">
                  </div>

                </div>
              
                <div class="row">
                  <div class="col-8" style="padding:0;margin:0;margin-top: 2rem;margin-left: 1rem;">
                  <?php echo $row9['address']; ?> 
                  </div>
                </div>
                <div style="text-align:left">
                <div class="row" style="margin-left: 1rem;">
                  <div class="col-4">
                    <p>No. of project completed</p>
                  </div>
                  <div class="col-4">
                    <P>Student Review</P>
                  </div>
                  <div class="col-4">
                    <p>Company Rating</p>
                  </div>
                </div>
                <div class="row" style="margin-left: 1rem;">

                  <div class="col-4"style="margin-top:-1rem">
                    <P><?php echo $row9['no_of_pro']; ?> </P>
                  </div>
                  <div class="col-4"style="margin-top:-1rem">
                    <p><?php echo $row9['student_review']; ?> </p>
                  </div>
                  <div class="col-4"style="margin-top:-1rem">
                    <p><?php echo $row9['company_rating']; ?> </p>
                  </div>
                </div>
                </div>
              
                <div class="row" style="margin-left:0;margin-right: -.5rem;">
                  <div class="col-12">
                    <h6>About <?php echo $row9['company_name']; ?> </h6>  
                  </div>   
                  <div class="col-12">
                    <p><?php echo $row9['company_desc']; ?> </p>
                  </div>         
                 
                </div>
                
                <div class="row" style="margin-left:0;margin-right: -.5rem;">
                    <div class="col-3  card">
                        <div class="row ml-2">Name</div>
                        <div class="row ml-2">descriptison</div>
                        <div class="row ml-2">Rating</div>
                    </div> 
                    <div class="col-3 ml-2 card">
                        <div class="row ml-2">Name</div>
                        <div class="row ml-2">descriptison</div>
                        <div class="row ml-2">Rating</div>
                    </div> 
                            

                </div>
                
              
                <div class="row justify-content-center mt-4" ">
                  <a href="requestpage.php?cid=<?php echo $row9['cid']; ?>"><input type="submit" class="btn text-light bg-dark" value="Apply" style="width: 15rem;"></a>
              </div>
                
              
              
              </div>
          
            </div>

              <?php }?>
        </diiv>
         
        </div>
      
    </section>
    
   <!-- ======= Footer ======= -->
   <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>