<!DOCTYPE html>
<html lang="en">

<?php 
  include("functionfiles/companyresponse.php");

  $rid = $_GET['rid'];
  echo "$rid";
  $findrecord1=mysqli_query($conn,"SELECT * FROM  requestpage where rid = '$rid'" );
  while ($row12=mysqli_fetch_array($findrecord1)){  
      $cid = $row12['cid'];
      $userid = $row12['userid'];
      $projecttitle = $row12['projecttitle'];
      $projectabstract = $row12['projectabstract'];
  
  
  
  }
?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
            <li><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li  class="active"><a href="companyprofile.php">Account</a></li>
              <li><a href="logout.php">Logout</a></li>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
        <div class="container-fluid signpage" style="margin-top: 1rem;">
           <div class="row">
               <div class="col-2">
                <div class="row card" style="margin-right: 1vh;">
                    <ul class="nav bg-dark">
                        <li class="nav-item">
                            <a href="companyprofile.php" class=" nav-link">Edit Profile</a>
                        </li>
                        <li class="nav-item">
                            <a href="companyprojectdetails.php" class=" nav-link">Projects Requests</a>
                        </li>
                        <li class="nav-item">
                            <a href="companypayments.php" class=" nav-link">Project Payments</a>
                        </li>
                        <li class="nav-item">
                          <a href="companybankdetails.php" class=" nav-link">Company Bank Details</a>
                      </li>
                      <li class="nav-item">
                      <a href="logout.php" class="btn text-light nav-link">Logout</a>
                    </li>
                    </ul>
                </div>
               </div>
               <div class="col-10">
                    <h4 class="text-center">PROJECT RESPONSE</h4>

                <div class="row card" style="margin-right: 1vh;">
         
              <form style="margin:1rem" action="functionfiles/companyresponse.php" method="POST">


                          <div class="form-group">
                          <label for="exampleFormControlSelect2">User id</label>
                            <input type="text" class="form-control" id="inputEmail4" placeholder="uid" name="uid" value="<?php echo $userid;?>" >
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect2">Company Id</label>
                            <input type="text" class="form-control" id="inputPassword4" placeholder="cid" name="cid" value="<?php echo $cid;?>" >
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect2">Project Title</label>
                            <input type="text" class="form-control" id="inputPassword4" placeholder="project Title" name="projecttitle" value="<?php echo $projecttitle;?> ">
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect2">Project Abstract</label>
                            <textarea type="text" class="form-control" id="inputPassword4" placeholder="Project Description" name="pbs" ><?php echo $projectabstract; ?></textarea>
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect2">Response</label>
                            <textarea type="text" class="form-control" id="inputPassword4" placeholder="Your Response" name="response" ></textarea>
                          </div>
                          <div class="form-group">
                          <label for="exampleFormControlSelect2">Total Fees</label>
                            <input type="text" class="form-control" id="inputPassword4" placeholder="Project Fees" name="tfees">
                          </div>

                          <div class="form-group">
                          <label for="exampleFormControlSelect2">Project Time Duration</label>
                            <input type="text" class="form-control" id="inputPassword4" placeholder="Project Duration" name="produration">
                          </div>
                        <button type="submit" class="btn bg-dark text-light" name="sendresponse">Send Response</button>
                        <button type="submit" class="btn bg-light " style="border:1px solid black">Cancel</button>
                      </form>
      
                  </div>
               </div>
           </div>
           
        </div>
      
    </section>
  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
 
  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="myscript.js"></script>
 
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>