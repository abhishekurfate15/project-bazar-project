$(function () {
    const category = [
        { value: "Computer Science Engineering", data: "Computer Science Engineering" },
        { value: "Electrical Engineering", data: "Electrical Engineering" },
        { value: "Electronic & Tele. Engineering", data: "Electronic & Tele. Engineering" },
        { value: "Mechanical Engineering", data: "Mechanical Engineering" },
        { value: "MBA / BBA", data: "MBA / BBA" },
        { value: "ME / MTECH", data: "ME / MTECH" },
        { value: "MCA / BCS / BCA", data: "MCA / BCS / BCA" },
    ];

    const indicators = [
        { value: "Pune", data: "Pune" },
        { value: "Mumbai", data: "Mumbai" },
        { value: "Nasik", data: "Nasik" },
        { value: "Aurangabad", data: "Aurangabad" },
    ]

    $("#input-countries").autocomplete({
        lookup: category,
        width: "flex",
    });

    $("#input-indicators").autocomplete({
        lookup: indicators,
        width: "flex",
    });

    
});