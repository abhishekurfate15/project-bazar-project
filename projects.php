<!DOCTYPE html>
<?php
  include("functionfiles/companyportfolio.php");
?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <?php 
              session_start();
              if(!isset($_SESSION["email"])){
                  
                  echo "<li><a href='logsign.php'>Sign IN / Sign UP</a></li>";
                }
                else{

                  $myemailhai = $_SESSION["email"];
                $searchuserdd=mysqli_query($conn,"SELECT * FROM userdetails where email = '$myemailhai'" );

                  while ($row=mysqli_fetch_array($searchuserdd)){
                    $usercat=$row['category'];

                    $usertag = 'User';
                    $companytag = 'Company';
                    if($usercat == $usertag){
    
                        // echo "<script>window.open('../profile.php','_self')</script>";

                        echo "<li><a href='profile.php'>Account</a></li>";
                    }
                    else if($usercat == $companytag){
                        // echo "<script>window.open('../companyprofile.php','_self')</script>";

                        echo "<li><a href='companyprofile.php'>Account</a></li>";
                        
                    }
                  }

                
                  echo "<li><a href='logout.php'>Logout</a></li>";

                }
              ?>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
       
        <div class="container-fluid">
          <div class="h4">Company</div>
          <diiv class="row">
           
            <div class="col-2 ml-3 mt-1" style="border:1px solid black;height:40vh">
              <div class="">
                <div class="row">
                    <div class="h5 ml-2 mt-3 font-weight-bold">Filter</div>
                </div>
                <form action="functionfiles/companyportfolio.php" method="post">
                  <div class="form-group">
                    <label for="formGroupExampleInput">Category</label>
                    <input type="text" class="form-control" name="inputcat" id="input-countries" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="formGroupExampleInput2">Location</label>
                    <input type="text" class="form-control" name="inputloc" id="input-indicators" placeholder="">
                  </div>
                  <div class="row justify-content-center mb-3 ">
                    <button type="submit" class="btn bg-dark text-light" name="sortdata">Search</button>

                  </div>
                </form>
              </div>
            </div>
            <div class="col-9 ml-3">
            <?php while ($row9=mysqli_fetch_array($resultdetails)){   ?>
           
              <div class="row">
             
              <div class="col-12 mt-1" style="border:1px solid black">
                  <div class="card" style="border:0">
                    <div class="row mt-3">
                      <div class="col-6">
                        <h4><?php echo $row9['company_name']; ?> </h4>
                      </div>
                      <div class="col-3"></div>
                      <div class="col-2 pull-right">
                          <img src="./functionfiles/images/<?php echo $row9['company_logo']; ?>" style="max-width:44%;border-radius:50px;float:right">
                      </div>
                    
                    </div>
                    <div class="row">
                      <div class="col-8" style="">
                          <i class="bx bx-map" style="font-weight: 100;font-size: 1.2rem;"></i>
                          <?php echo $row9['address']; ?>
                      </div>                
                    </div>
                    <div class="row mt-3 ">
                        <div class="col-4">
                          <p>No. of project completed</p>
                          <p class="ml-3"><?php echo $row9['no_of_pro']; ?></p>
                        </div>
                        <div class="col-4">
                          <P>Student Review</P>
                          <P class="ml-3"><?php echo $row9['student_review']; ?></P>                      
                        </div>
                        <div class="col-4">
                          <p>Company Rating</p>
                          <p class="ml-3">
                          <?php 
                            $count =$row9['company_rating'];
                        
                            for ($x = 1; $x <= $count; $x++) {
                              echo " <i class='fa fa-star'></i>";
                            }
                          
                          
                          ?></p>


                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-12">
                          <h6>About <?php echo $row9['company_name']; ?> </h6>              
                        </div>
                        <div class="col-11" style="max-width:70%">
                            <p class="text-wrap"><?php echo $row9['company_desc']; ?>.</p>
                        </div>
                    </div>

                    <div class="row justify-content-end mb-3 mr-3"  >
                        <a href="projectdetails.php?cid=<?php echo $row9['cid']; ?>" style="color: blue;">View details<i class="bx bx-chevron-right"></i></a>
                    </div>
                  </div>
              </div>
              </div>
             
          <?php }?>
          </div>
          </diiv>
         
        </div>
      
    </section>
  
    <!-- ======= Footer ======= -->
    <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

    <script src="assets/vendor/jquery-autocomplete.1.4.11.js"></script>
    <script src="assets/js/autocomplete-dropdown.js"></script>


  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>