<!DOCTYPE html>
<?php
  include("functionfiles/adminfunc.php");
?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
<style>
    table tr td{
        padding:.5rem;
    }
</style>
</head>



<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
           
               
            <li class="active"><a href="index.php">Home</a></li>
              <li><a href="userdetails.php">User Details</a></li>              
              <li><a href="companydetails.php">Company Details</a></li>
              <li><a href="logout.php">Logout</a></li>


            </ul>
          </nav><!-- .nav-menu -->
          
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
        <div class="container-fluid signpage" style="margin-top: 1rem;">
       
           <div class="row">
               <div class="col-2">
                <div class="row card" style="margin-right: 1vh;">
                  <ul class="nav bg-dark">
                    <li class="nav-item">
                        <a href="#" class=" nav-link user-btn">User Details</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class=" nav-link request-btn">All Requests</a>
                    </li>
                
                    <li class="nav-item">
                        <a href="#" class=" nav-link account-btn">Account Details</a>
                    </li>
                 
              
                </ul>
                </div>
               </div>
               <div class="col-10 request-table">
                    <h4 class="text-center">User Details</h4>
                <?php

                                
                $allproquery1="SELECT * FROM requestpage";
                                
                $resultdetails1=mysqli_query($conn,$allproquery1);
                
                ?>
                <div class="row card" style="margin-right: 1vh;">
             
                <table class="table-bordered table-hover">
                <thead class="bg-dark text-light">
                        <tr>
                            <td>Sr.No</td>
                            <td>Request no.</td>
                            <td>user-id</td>
                            <td>Company Name</td>
                            <td>Project Title</td>
                            <td>Project Abstract</td>
                            <td>Date of join</td>
                        </tr>
                    </thead>
                    <tbody>

                    <?php 
                       $count = 1;
                        while ($row1=mysqli_fetch_array($resultdetails1)){  
                         
                      ?>
                          <tr>
                          <td><?php echo $count; ?></td>
                          <td><?php echo $row1['cid']; ?></td>
                          <td><?php echo $row1['userid']; ?></td>
                          <td><?php echo $row1['company_name']; ?></td>
                          <td><?php echo $row1['projecttitle']; ?></td>
                          <td><?php echo $row1['projectabstract'];?></td>
                          <td><?php echo $row1['date'];?></td>
                        
                          </tr>

                        <?php 
                        $count = $count +1;
                    }
                        ?>
                    </tbody>
                </table>
                 
              
                  </div>
               </div>

               <div class="col-10 user-table">
                    <h4 class="text-center">Request Details</h4>
                <?php

                                
                $allproquery="SELECT * FROM userdetails";
                                
                $resultdetails=mysqli_query($conn,$allproquery);
                
                ?>
                <div class="row card" style="margin-right: 1vh;">
             
                <table class="table-bordered table-hover">
                <thead class="bg-dark text-light">
                        <tr>
                            <td>Sr.No</td>
                            <td>user-id</td>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Email id</td>
                            <td>Password</td>
                            <td>Address</td>
                            <td>City</td>
                            <td>Category</td>
                            <td>Mobile Number</td>
                            <td>Date of join</td>
                        </tr>
                    </thead>
                    <tbody>

                    <?php 
                       $count = 1;
                        while ($row9=mysqli_fetch_array($resultdetails)){  
                         
                      ?>
                          <tr>
                          <td><?php echo $count; ?></td>
                          <td><?php echo $row9['uid']; ?></td>
                          <td><?php echo $row9['fname']; ?></td>
                          <td><?php echo $row9['lname']; ?></td>
                          <td><?php echo $row9['email'];?></td>
                          <td><?php echo $row9['pass'];?></td>
                          <td><?php echo $row9['address'];?></td>
                          <td><?php echo $row9['city'];?></td>
                          <td><?php echo $row9['category'];?></td>
                          <td><?php echo $row9['mobile'];?></td>
                          <td><?php echo $row9['date'];?></td>
                        
                          </tr>

                        <?php 
                        $count = $count +1;
                    }
                        ?>
                    </tbody>
                </table>
                 
              
                  </div>
               </div>

               <div class="col-10 account-table">
                    <h4 class="text-center">Account Details</h4>
                <?php

                                
                $allproquery3="SELECT * FROM myuser_payments";
                                
                $resultdetails3=mysqli_query($conn,$allproquery3);
                
                ?>
                <div class="row card" style="margin-right: 1vh;">
             
                <table class="table-bordered table-hover">
                <thead class="bg-dark text-light">
                        <tr>
                            <td>Sr.No</td>
                            <td>user-id</td>
                            <td>Project Title</td>
                            <td>Company Name</td>
                            <td>Status</td>
                            <td>Total Fees</td>
                            <td>Paid Fees</td>
                            <td>Remain Fees</td>
                            <td>Date</td>
                        </tr>
                    </thead>
                    <tbody>

                    <?php 
                       $count = 1;
                        while ($row2=mysqli_fetch_array($resultdetails3)){  
                         
                      ?>
                          <tr>
                          <td><?php echo $row2['pyid']; ?></td>
                          <td><?php echo $row2['uid']; ?></td>
                          <td><?php echo $row2['pro_name']; ?></td>
                          <td><?php echo $row2['company_name']; ?></td>
                          <td><?php echo $row2['status']; ?></td>
                          <td><?php echo $row2['tfees']; ?></td>
                          <td><?php echo $row2['pfees']; ?></td>
                          <td><?php echo $row2['rfees']; ?></td>
                          <td><?php echo $row2['date']; ?></td>
                       
                        
                          </tr>

                        <?php 
                        $count = $count +1;
                    }
                        ?>
                    </tbody>
                </table>
                 
              
                  </div>
               </div>
           </div>
        

        </div>

    </section>
  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="myscript.js"></script>
  <script>
      $(".user-table").show();
        $(".request-table").hide();
        $(".account-table").hide();
    $(".user-btn").on("click", (e) => {
        $(".user-table").show();
        $(".request-table").hide();
        $(".account-table").hide();
    });

    $(".request-btn").on("click", (e) => {

        $(".user-table").hide();
        $(".request-table").show();
        $(".account-table").hide();
    });

    $(".account-btn").on("click", (e) => {
        $(".user-table").hide();
        $(".request-table").hide();
        $(".account-table").show();
    });

 </script>
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>