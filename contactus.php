<!DOCTYPE html>
<html lang="en">
<?php
  include("functionfiles/getintouch.php");
?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href="logsign.php">Sign IN / Sign UP</a></li>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
        <h4 class="text-center">Contact Us</h4>
        <div class="container">
            <div class="row" style="margin-top: 2rem;">
                <div class="col-6 text-center">
                    <h4>Phone & Email</h4>
                    <p>+91 7888117202</p>
                    <p style="margin-top: -1rem;">projectbazar02@gmail.com</p>
                </div>
                <div class="col-6  text-center">
                    <h4>Working & Hour</h4>
                    <p>Monday to Saturday - 10am to 7pm</p>
                    <p  style="margin-top: -1rem;">Sunday - Closed</p>
                </div>
               
            </div>
        </div>
      <div class="container" style="color: black;margin-top: 3rem;">
        <div class="row">
           
            <div class="col-12 text-center card shadow"style="margin-left: .5rem;border:0.1px solid black">
                <h4 style="margin-top: .5rem;">Get in Touch</h4>
                <p>Please fill out the information below. Alternatively, you may call or send us an email using our contact details.</p>
                <p>Asterisks (*) indicates fields to complete this transaction.</p>

                <form style="margin:1rem" action="functionfiles/getintouch.php" method="post">
                    <div class="">
                      <div class="form-group">
                        <input type="text" class="form-control" id="inputEmail4" name="username" placeholder="Name">
                      </div>
                    
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control" id="inputAddress" name="contactno" placeholder="Contact">
                    </div>
                    <div class="form-group">
                      <input type="emal" class="form-control" id="inputAddress2" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" style="height:13vh" class="form-control" name="datas"  id="inputAddress2" placeholder="Enquiry">
                      </div>
                      <div class="form-group">
                        <button type="submit" style="width:100%" name="sendfeedback" class="btn btn-dark">submit</button>
                      
                    </div>
                
                  
                  </form>
            </div>
        </div>
      </div>
    </section>
  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>