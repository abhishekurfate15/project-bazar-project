<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href="logsign.php">Sign IN / Sign UP</a></li>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about" style="color: black;">
      <div class="container">
          <div class="row" style="margin-top: 1rem;">
            
          </div>
          <div class="row" style="margin-top: -.8rem;">
           
          </div>
          <h4>Privacy Notice </h4>
        <p>We know that you care how information about you is used and shared, and we appreciate your trust that we will do so carefully and sensibly. This Privacy Notice describes how Project Bazar collects and processes your personal information through project Bazar websites, online marketplace, and applications that reference this Privacy Notice. Our Services may contain links to other websites over which we have no control and we are not responsible for the privacy policies or practices of other websites to which you navigate from our Services. We encourage you to review the privacy policies of these other websites so you can understand how they collect, use, and share your information. This privacy statement applies solely to the information we collect on internshala.com and its sub-domains and not to information collected otherwise.</p>

        <h4>Collection of Information</h4>
        <p>Personal information provided by you:  We request you to provide or submit personal information including your name, address, email address, telephone number, contact information, billing information, education details, workplace details, and any other information from which your identity is discernible. Internshala also collects or may collect demographic information that is not unique to you such as your ZIP code, age, preferences, and gender. An example of such a situation could include but is not limited to when you sign up to use our service, post a resume, or enter a contest or survey. </p>
        <p>Information from Other Sources: We might receive information about you from other sources, such as updated delivery and address information from our carriers, which we use to correct our records and deliver your next purchase more easily. </p>
        <p>Information collected when you use third-party services: For a better experience, and in order to provide our service, we may require you to provide us certain personally identifiable information, including but not limited to the user name. The information that we request will be retained by us and used as described in this privacy policy. Project Bazar uses third-party services that may collect information used to identify you. Links to privacy policies of third party service providers used by us are </p>
        <p>Google Sign In</p>
        <p>We may collect, process, and store your information associated with your Google account if you choose to sign in using Google. When you sign in to your account with your Google account information, you consent to our collection, storage, and use of the information that you make available to us through your Google account in accordance with this Privacy Policy</p>
        <p>Information collected through the use of service: We also gather or may gather certain information about your use of our Services, such as what areas you visit and what features you access. Moreover, there is information about your computer hardware and software that is or may be collected by us. This information can include without limitation your IP address, browser type, domain names, access times, and referring website addresses and can be used by Internshala as it deems fit. If you message applicants through the Internshala chat platform, we also collect and may access your chat history.</p>
          
        <h4>Retention of Information</h4>
        <p>Since we believe that managing your career is a life-long process, we retain indefinitely all the information we gather about you in an effort to make your repeat use with us more efficient, practical, and relevant. You can correct or update your account profile and application at any time. You may choose to delete your account, following which we will either delete or de-identify the data.</p>

        <h4>Sharing of information</h4>
        <p>If you are an employer, information related to your organization post such as organization name, URL, and description, project description, skills required, etc. is published online and can be viewed by anyone visiting project bazar site. This information may also appear in search websites like Google. Further, we may share your personal information (including contact details) with students who apply to your organization through project bazar.</p>
        <p>If you are an applicant, we may share your personal information with employers whose organization you apply for a project, we feel may be relevant for you or who may come across your profile through a search of our user base.</p>
        <p>We may share your information with third-party individuals, contractors, companies, and other service providers that perform services on our behalf or in partnership with us. For example, we may require to pass on your data to a partner who might be responsible for the delivery of a project that you may have voluntarily signed up for.</p>
        <p>Additionally, you may have the option to share your information and activities on project bazar through social media. We do not have control over information shared with third-party social networks and you should review the policies of those networks before consenting to share information with them.</p>
        <p>If you post any of your personal information in public areas of project bazar, such as in online forums, chat rooms, comments section etc. this information will be accessible to everyone on the public internet. It may be collected and used by others over whom project bazar has no control. We are not responsible for the use made by third parties of information you post or otherwise make available in public areas of this website.</p>
        <p>When we send you an email or SMS, we use a third party service. In this case, it becomes necessary to pass on your email address/ mobile number to the third party. While we only work with reputed service providers in this regard, we are not responsible for the use made by them of this information.</p>
        <p>Project bazar reserves the right to publish the internship and/or job post of companies on social media platforms and 3rd party websites like LinkedIn, Indeed, Sheroes, Qween, etc.along with newspapers and magazines, in order to increase the visibility of the post.</p>
        <p>We may also share your information in order to investigate, prevent, or take action about possible illegal activities or to comply with legal process. We may also share your information with third parties to investigate and address violations of this Privacy Policy or the Terms of Service, or to investigate and address violations of the rights of third parties and/or project bazar, our employees, users, or the public. This may involve the sharing of your information with law enforcement, government agencies, courts, and/or other organizations.</p>

        <h4>Downloading information</h4>
        <p>Our customer support team will be happy to help you with this. Please write to us at <span style="color: blue;" >projectbazar02@gmail.com</span></p>

        <h4>Cookies and pixel tags</h4>
        
        <p>We and third parties with whom we partner, may use cookies, web beacons, pixel tags etc. to collect information regarding your use of project bazar and third party websites. A cookie is a small text file that is stored on your computer that enables us to remember you (for example, as a registered user) and your actions when you visit our website. This helps us remember your preferences and searches, improve your experience (for example, by keeping you logged in), customize content according to your preferences and perform analytics, and assist with security and administrative functions. Cookies may be persistent or stored only during an individual session ,as necessary. A pixel tag is a tiny graphic with a unique identifier embedded invisibly in an email and may be used to track whether the email has been opened and for other analytics.</p>
        <p>Most modern browsers will allow you to disable some/all cookies for any website. However, this is not recommended and doing so may interfere with normal functioning of project bazar. If you do not turn cookies off, please make sure you logout when you finish using a shared computer.</p>

        <h4>Children</h4>
        <p>Internshala is not intended for children under 13 years of age. If you are less than 13 years old at the time of your first visit to Internshala, you are prohibited from using the website further entirely on your own. You may do so under parental guidance. However, please note that we have no way of determining your age when you visit our website or whether you have parental supervision available or not. We do not intend to and do not knowingly collect personal information from children under 13. However, since we do not collect information on a user's birth date and proof of same, there is no foolproof way for us to ensure the same.</p>
        <h4>Security of information</h4>
        <p>We have implemented generally accepted industry standards in terms of security measures to protect your information on project bazar. The third party payment service providers (payment gateways) are all validated as compliant with the payment card industry standard (generally referred to as PCI compliant service providers).</p>
            <p>While we try our best to safeguard information, there may be factors beyond our control that may result in unwanted disclosure of information. We assume no liability or responsibility for disclosure of your information due to causes beyond our control.</p>
                <p>In order to keep personal information secure, you must not share your password or other security information (for example, unique keys) of your project bazar account with anyone. If you are using a shared computer, please make sure you logout after every use. If we receive instructions using your email and password, we will consider that the instructions have been authorized by you.</p>
        <h4>No Guarantees</h4>
        <p>While this Privacy Policy states our standards for maintenance of data and we will make efforts to meet them, we are not in a position to guarantee these standards. There may be factors beyond our control that may result in the disclosure of data. As a consequence, we disclaim any warranties or representations relating to maintenance or nondisclosure of data.</p>

        <h4>Changes to this Privacy Policy</h4>
        <p>We may make changes to this Policy from time to time. We may notify you of substantial changes to this Policy either by posting a prominent announcement on our Services and/or by sending a message to the e-mail address you have provided to us. You are advised to refer to this page to know about our latest Privacy Policy.</p>

      </div>
    </section>

  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>