<!DOCTYPE html>
<?php
  include("functionfiles/userpro.php");
?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
            <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href="profile.php">Account</a></li>
              <li><a href="logout.php">Logout</a></li>
            

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about"  style="color: black;margin-top: 3rem;">
        <div class="container-fluid signpage" style="margin-top: 1rem;">
           <div class="row">
               <div class="col-1">
                <div class="row card myprofile bg-dark" style="padding-right: 1vh;">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="profile.php" class="btn text-light nav-link">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a href="myproject.php" class="btn text-light nav-link">Projects</a>
                        </li>
                        <li class="nav-item">
                            <a href="prohistory.php" class="btn text-light nav-link">History</a>
                        </li>
                        <li class="nav-item">
                            <a href="mypayment.php" class="btn text-light nav-link">Payments</a>
                        </li>
                        <li class="nav-item">
                            <a href="logout.php" class="btn text-light nav-link">Logout</a>
                        </li>
                        <li class="nav-item">
                            <a href="faq.php" class="btn text-light nav-link">FAQ</a>
                        </li>
                        <li></li>
                    </ul>
                </div>
               </div>
               <div class="col-10" style="margin-left:.4rem">
                    <h4 class="text-center">PROJECTS DETAILS</h4>

                <div class="row card" style="margin-right: 1vh;">
         
                    <table class="table border">
                        <tr class="headling">
                            <th>Sr. No</th>
                            <th>Project Name</th>
                            <th>Company ID</th>
                            <th>Company Name</th>
                            <th>Company Response</th>
                            <th>Status</th>
                            <th>Duration</th>
                            <th>profees</th>
                            <th>Make Payments</th>
                            <th>Date</th>
                           
                        </tr>
                      <?php 
                       $count = 1;
                        while ($row9=mysqli_fetch_array($queimypro)){  
                         
                      ?>

                          <tr>
                          <td><?php echo $count; ?></td>
                          <td><?php echo $row9['protitle']; ?></td>
                          <td><?php echo $row9['cid']; ?></td>
                          <td><?php echo $row9['company_name']; ?></td>
                          <td><?php echo $row9['response'];?></td>
                          <td><?php echo $row9['status'];?></td>
                          <td><?php echo $row9['produration'];?></td>
                          <td><?php echo $row9['profees'];?></td>
                          <td><a href="paymentnow.php" class="btn text-light btn-primary btn-sm mt-1">Pay Now</a></td>
                          <td><?php echo $row9['date'];?></td>
                        
                          </tr>
                          

                         <?php

                            $count = $count +1;
                          }
                      ?>
                       
                    </table>
      
                  </div>
               </div>
           </div>
           
        </div>
      
    </section>
  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
 
  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="myscript.js"></script>
 
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>