<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Project Bazar</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-color: black;">
    <div class="container-fluid">

      <div class="row">
        <div class="col-1"></div>

        <div class="col-2">
          <div class="row logo">
            <div class="col-5">
              <a  href="index.php">
              <img src="./assets/img/logopb.png" >
         </a>
            </div>
          </div>
          
        </div>
         <div class="col-3"></div>
        <div class="col-4">
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="projects.php">Project</a></li>              
              <li><a href="contactus.php">Contact</a></li>
              <li><a href="logsign.php">Sign IN / Sign UP</a></li>

            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about" style="color: black;">
      <div class="container">
        <div class="row mt-3">
          <div class="col-12 text-center">
          <h3 class="font-weight-bold">Terms & Conditions</h3>
          </div>
        </div>  

        <div class="row mt-4">
          <p class="font-weight-bold h4">Most Important Terms & Conditions for Employers</p>
          <p class="font-weight-bold">These Terms, specifically for Employers, are in addition to the other generic Terms & Conditions listed on the page which are common to all the users of the site and which you are required to go through. Any violation of these Terms may result in, but not limited to, an Employer's Job being declined for publication on projectbazar and/or his/her account being permanently blocked and/or appropriate legal action being initiated against him/her by projectbazar.</p>
        </div>

        <div class="row mt-2">
          <p class="h5 font-weight-bold">Posting Company details T&Cs:</p>
          <div class="row">
            <div class="col ml-2">
              <p class="h5">1.	It is your responsibility to ensure that you are authorized to post on your organization's behalf. Any dispute or legal claim arising out of unauthorized posting would be solely your liability and you reimburse projectbazar of any and all possible consequences of such actions.</p>
              <p class="h5">2.	If you upload your organization's logo while creating/updating your company profile, you are authorizing projectbazar to display it on our website along with company detail or in the list of our clients. It is your responsibility to ensure that you are duly authorized to share your organization logo with 3rd parties and allow them to use it.</p>
              <p class="h5">3.	You must provide accurate and complete details about the organization. Any act of misinformation or hiding material information would result in your account being permanently blocked or any other suitable action as deemed fit by projectbazar.</p>
              <p class="h5">4.	You may be asked for documents in the name of your company for our first-time user authentication process. This information will only be used for authentication purposes and will not be used anywhere else in any form.</p>
          
            </div>    
          </div>
        </div>

        <div class="row mt-2">
          <p class="h5 font-weight-bold">Approving T&Cs:</p>
          <div class="row">
            <div class="col ml-2">
              <p class="h5">1.	It is your responsibility to ensure that there is no material difference between the company details that are advertised on projectbazar and the details that are communicated to Applicants later in the approving process. Any ‘negative' material difference (such as offered value is lower than what was advertised) will result in suitable action being taken by projectbazar against you.</p>
              <p class="h5">2.	All your communications with Applicants (through projectbazar Chat or otherwise) should be professional and must not contain any obscene or offensive content.</p>
              <p class="h5">3.	Once you approved any Applicant(s) for the project, you must provide them clearly detailing all the important details of the project (such as completion date & price of the project) and the complete address and contact details of your organization.</p>
              <p class="h5">4.	You must respond within 72 working hours to any Applicant complaints regarding the project that we may bring to your notice. Failure to do so may result in a temporary or permanent suspension of your projectbazar account depending on the nature of the complaint.</p>
              <p class="h5">5.	If you come across any suspicious activity, content, or behavior on projectbazar by an Applicant or another user, you must report it to projectbazar immediately so that necessary actions can be taken.</p>
              <p class="h5">6.	While we put our best efforts to reach out to the best talent available in the country, posting your organization on projectbazar does not guarantee to get the project.</p>
          </div>
          </div>          
        </div>


        <div class="row mt-2">
          <p class="h5 font-weight-bold">Applicant data usage T&Cs:</p>
          <div class="row">
            <div class="col ml-2">
            <p class="h5">1.	You can use the Applicants' data that you receive for completing the project. Any attempt to send any other communication (promotional content for example) to the Applicants or any other usage of the data is strictly prohibited.</p>
            <p class="h5">2.	You are strictly prohibited from transferring/selling/sharing Applicants' data, for free or for a fee, to any other entity. Any such attempt would result in your account on projectbazar being permanently blocked and would be liable for legal action against you and your organization.</p>
            <p class="h5"></p>
          </div>
          </div>          
        </div>


        <div class="row mt-2">
          <p class="h5 font-weight-bold">Payment & refund T&Cs:</p>
          <div class="row">
          <div class="col ml-2">
            <p class="h5">1.	Refund if any will be as per our Refund Policy.</p>
            <p class="h5">1.	Projectbazar  offers no guarantees whatsoever for the accuracy or timeliness of the refunds reaching the Employers card/bank accounts.</p>
            <p class="h5"></p>
          </div>  
          </div>          
        </div>


        <div class="row mt-2">
          <p class="h4 font-weight-bold">Important Terms & Conditions for Students</p>
          <div class="row">
          <div class="col ml-2">
            <p class="h5">These Terms, specifically for Students, are in addition to the other generic Terms & Conditions listed on the page which are common to all the users of the site and which you are required to go through. Any violation of these T&Cs may result in, but not limited to, an applicant's Job application being declined and/or his/her account being permanently blocked and/or suitable legal action being initiated against him/her by Projectbazar.</p>
            <p class="h5">1.	If you have applied to a company on projectbazar and receive a communication from projectbazar or Employer regarding your application, you must respond to it with-in 72 hours.</p>
            <p class="h5">2.	Once you finalized the project with the company , you must make every possible effort to make it successful. To not show up or to decline an earlier finalized project at the last moment, or to go incommunicado creates a very poor impression of you in front of the Employer and reduces the credibility of the student community at large. Any such behaviour will not only result in your account being blocked on projectbazar but we will also report it to your college administration wherever applicable.</p>
            <p class="h5">3.	You must provide accurate and complete information at the time of project requesting application or creating an account on projectbazar – any misrepresentation of information, or hiding of material information, or impersonation would result in your account being blocked on the site and you being reported to your college administration wherever applicable.</p>
            <p class="h5">4.	You must pay attention to a organization’s complete details (past project, student review, remuneration, etc.) before applying for it. Irrelevant applications will result in your account being penalized (we may stop you from applying to future Jobs or block your account all together)</p>
            <p class="h5">5.	You are strictly prohibited from transferring/selling/sharing Employers' data (contact details etc.), for free or for a fee, with any other entity. Any such attempt would result in your account on projectbazar being permanently blocked and would be liable for legal action against you.</p>
            <p class="h5">6.	Projectbazar community is a community of sincere users who are known for and are expected to demonstrate high standards of professionalism in their dealings with other users and Employers – you are required to maintain these standards and decorum of the community. All your communications with other users of projectbazar (through projectbazar Chat or otherwise) should be professional and must not contain any obscene or offensive content.</p>
            <p class="h5">7.	While it is our endeavor to provide you with accurate and reliable company; it is always possible for an error to occur. Hence, you must conduct your own due diligence and research about an employer or organisation before finalizing the project and take full responsibility of the same. You further agree to have read and understood the ‘Disclaimers' section of these Terms and explicitly and specifically agree to it.</p>
            <p class="h5">8.	If you come across any suspicious activity, content or behaviour on projectbazar by an Employer or another user, you must report it to projectbazar immediately so that necessary actions can be taken – this is both your duty and obligation.</p>
            <p class="h5">9.	While we make our best efforts to bring you the best project opportunities possible.</p>
            <p class="h5">10.	Upon registration, your registered email id and phone number will be automatically subscribed to receive email and SMS notifications from projectbazar. You may opt out of it anytime.</p>
          </div>
          </div>          
        </div>


        <div class="row mt-2">
          <p class="h5 font-weight-bold">It is mandatory for Employers to ensure that :</p>
          <div class="row">
          <div class="col ml-2">
            <p class="h5">1.	Project details communicated to Applicants at any stage are same as requested by the applicant on ProjectBazar and there is no negative material difference.</p>
            <p class="h5">2.	All their communications with Applicants (through ProjectBazar Chat or otherwise) are professional and must not contain any obscene or offensive content.</p>
            <p class="h5">3.	Once they accept any Applicant(s) for their project, they must provide them with all the important details of the Project.</p>
            <p class="h5">If you come across any example of any Employer violating these guidelines, you must report it to ProjectBazar by dropping a mail so that we can investigate and take appropriate actions.</p>
          </div>          
          </div>
        </div>



        <div class="row mt-2">
          <p class="h5 font-weight-bold">Termination</p>
          <div class="row">
          <div class="col ml-2">
            <p class="h5">Notwithstanding any of these Site Terms, SEPL reserves the right, without notice and in its sole discretion, to terminate your account and/or to block your use of the Site.</p>
          </div>    
          </div>      
        </div>



        <div class="row mt-2">
          <p class="h5 font-weight-bold">Changes to Site Terms</p>
          <div class="row">
          <div class="col ml-2">
            <p class="h5">ProjectBazar reserves the right to change any of the terms and conditions contained in the Site Terms or any policy or guideline of the Site, at any time and in its sole discretion. Any changes will be effective immediately upon posting on the Site. Your continued use of the Site following the posting of changes will constitute your acceptance of such changes. We encourage you to review the Site Terms whenever you visit the website.            </p>
            </div>          
            </div>          
        </div>


        <div class="row mt-2">
          <p class="h5 font-weight-bold">Jurisdiction</p>
          <div class="row">
          <div class="col ml-2">

            <p class="h5">All license agreements, use or any issues arising out of any activity regarding the use of this website will be governed by the laws of India and subject to the exclusive jurisdiction of courts in Pune.</p>
          </div>          
          </div>
        </div>


      </div>       
    </section>

  <!-- ======= Footer ======= -->
  <?php  include("functionfiles/footer.php"); ?>
  <!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>